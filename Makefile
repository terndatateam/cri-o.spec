
.PHONY: build rpm

build:
	docker run --rm -it -v $(CURDIR):/root/rpmbuild -w /root/rpmbuild centos:8 bash -c 'dnf install -y make; make rpm'

# https://stackoverflow.com/questions/33177450/how-do-i-get-rpmbuild-to-download-all-of-the-sources-for-a-particular-spec
rpm:
	dnf -y update
	dnf install -y rpm-build 'dnf-command(builddep)' 'dnf-command(config-manager)' epel-release
	dnf config-manager -y --set-enabled PowerTools
	dnf builddep -y SPECS/cri-o.spec
	rpmbuild --undefine=_disable_source_fetch -ba SPECS/cri-o.spec
