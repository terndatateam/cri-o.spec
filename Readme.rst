
CRIO-O RPM
==========

This repo contains the rpm sources to build cri-o for CentOS 8.


Build
-----

The Makefile is set up to start a CentOS 8 container and build the rpm inside this container.

.. code:: bash

    make build

After that the built rpm should be in the `./RPMS/x86_64`. The rpm can then be uploaded to bitbucket downloads section of this repo, from where it can be installed directly via `dnf install <url>`.


